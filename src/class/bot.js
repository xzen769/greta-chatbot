const Bot = class Bot {
  constructor(entity) {
    const {
      id, name, avatar, count, actions
    } = entity;

    this.id = id;
    this.name = name;
    this.avatar = avatar;
    this.count = count;
    this.actions = actions;
  }

  findActionsByKeyword(keyword) {
    return this.actions
      .filter((action) => action.keywords.includes(keyword))
      .map((action) => action.action());
  }
};

export default Bot;
