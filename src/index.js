import ControllerTchat from './controllers/tchat';

import './index.scss';

const bots = [{
  id: '1',
  name: 'spiderman',
  avatar: 'https://avatarfiles.alphacoders.com/149/thumb-149117.jpg',
  count: 0,
  actions: [{
    name: 'hello',
    keywords: ['hello', 'bonjour'],
    action: () => 'Salut !'
  }]
}, {
  id: '2',
  name: 'wonder woman',
  avatar: 'https://avatarfiles.alphacoders.com/167/167527.jpg',
  count: 0,
  actions: [{
    name: 'hello',
    keywords: ['hello', 'bonjour'],
    action: () => 'Salut !'
  }, {
    name: 'date',
    keywords: ['date', 'time'],
    action: () => new Date()
  }]
}];

new ControllerTchat(bots);
