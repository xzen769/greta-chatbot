export default (data) => `
  <div class="row mb-3">
    <div class="col-6">
      <div class="card">
        <div class="card-header">
          <img
            width="40"
            src="${data.avatar}"
            class="img-thumbnail rounded-circle"
            alt="avatar"
          >
          ${data.name}
        </div>
        <div class="card-body">
          <h5 class="card-title">${data.created_at}</h5>
          <p class="card-text">${data.txt}</p>
        </div>
      </div>
    </div>
    <div class="col-6"></div>
  </div>
`;
