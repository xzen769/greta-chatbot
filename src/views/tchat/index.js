import viewBot from './bot';
import viewNav from './nav';
import viewMessageReceived from './message-received';
import viewMessageSended from './message-sended';

export default (data) => {
  const { bots, messages } = data;

  return `
    ${viewNav()}
    <div class="container-fluid">
      <div class="row">
        <div class="col-3">
          <ul class="list-group list-group-flush">
            ${bots.map((bot) => viewBot(bot)).join('')}
          </ul>
        </div>
        <div class="col-9 pt-2">
          <div class="messages">
            ${messages.map((message) => (message.type === 'bot' ? viewMessageReceived(message) : viewMessageSended(message))).join('')}
          </div>
          <div class="row">
            <div class="input-group mb-3">
              <input type="text" class="form-control" placeholder="Send message" aria-label="Recipient's username" aria-describedby="button-addon2">
              <button class="btn btn-primary" type="button" id="button-addon2">Button</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  `;
};
