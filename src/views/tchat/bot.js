export default (data) => `
  <li class="list-group-item d-flex justify-content-between align-items-center">
    <img width="60" src="${data.avatar}" class="img-thumbnail rounded-circle" alt="avatar">
    ${data.name}
    <span class="badge bg-primary rounded-pill">${data.count}</span>
  </li>
`;
