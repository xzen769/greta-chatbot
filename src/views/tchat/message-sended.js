export default (data) => `
  <div class="row mb-3">
  <div class="col-6"></div>
    <div class="col-6">
      <div class="card">
        <div class="card-header">
          <img
            width="40"
            src="https://media.licdn.com/dms/image/C5603AQFB2qvfRVtynQ/profile-displayphoto-shrink_200_200/0/1540912243495?e=1691020800&v=beta&t=nKXDrsVjW7TBp8K42j-7jJkNCKgy9dtqf6Vl8Vd2MiA"
            class="img-thumbnail rounded-circle"
            alt="avatar"
          >
          Spiderman
        </div>
        <div class="card-body">
          <h5 class="card-title">${data.created_at}</h5>
          <p class="card-text">${data.txt}</p>
        </div>
      </div>
    </div>
  </div>
`;
