import axios from 'axios';

import Bot from '../class/bot';
import viewTchat from '../views/tchat';
import viewMessageSended from '../views/tchat/message-sended';
import viewMessageReceived from '../views/tchat/message-received';

const Tchat = class Tchat {
  constructor(bots) {
    this.el = document.body;
    this.data = {
      bots: bots.map((bot) => new Bot(bot)),
      messages: []
    };

    this.run();
  }

  getResponsesByBots(keyword) {
    const { bots } = this.data;
    let responses = [];

    bots.forEach((bot) => {
      const response = bot.findActionsByKeyword(keyword);

      if (response.length) {
        responses = responses.concat(response.map((txt) => ({
          type: 'bot',
          botId: bot.id,
          created_at: new Date().toISOString(),
          txt
        })));
      }
    });

    return responses;
  }

  formatBotMessages(bots, messages) {
    return messages.map((message) => ({
      ...message,
      ...bots.find((bot) => bot.id === message.botId)
    }));
  }

  onHandleSendMessage() {
    const elInput = document.querySelector('input');
    const elMessages = document.querySelector('.messages');
    const { bots } = this.data;

    elInput.addEventListener('keypress', (e) => {
      if (e.keyCode === 13) {
        const keyword = elInput.value;

        if (!keyword) {
          return;
        }

        const message = {
          type: 'guest',
          created_at: new Date().toISOString(),
          txt: keyword
        };

        axios.post('http://localhost:3000/messages', message);

        elMessages.innerHTML += viewMessageSended(message);
        const responses = this.getResponsesByBots(keyword);
        const responsesFormated = this.formatBotMessages(bots, responses);
        elMessages.innerHTML += responsesFormated
          .map((response) => (viewMessageReceived(response)))
          .join('');
        elMessages.scrollTop = elMessages.scrollHeight;
        elInput.value = '';
      }
    });
  }

  run() {
    const { bots } = this.data;

    axios.get('http://localhost:3000/messages')
      .then((response) => {
        const { data } = response;
        const messages = data;
        this.data.messages = this.formatBotMessages(bots, messages);

        this.el.innerHTML = viewTchat(this.data);
        const elMessages = document.querySelector('.messages');
        elMessages.scrollTop = elMessages.scrollHeight;
        this.onHandleSendMessage();
      });
  }
};

export default Tchat;
